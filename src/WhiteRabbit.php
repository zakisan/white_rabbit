<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
	private function parseFile ($filePath)
	
	{

    //intiering af variabler
	$charlist = [];
	$ch1 = "a";
	$ch2 = "A";

	$filepath = "txt/text1.txt";
	//indl�s tekstfil til string
	$file = file_get_contents($filepath);

	
	// Loop for at l�be bogstaverne 'a to z' igennem
	for($i = 1; $i <= 26; $i++) {

		//finder antal sm� og store bogstaver i tekstfilen
		$count = preg_match_all("/".$ch1."/", $file, $matches)+preg_match_all("/".$ch2."/", $file, $matches);
		//echo $ch1 . ' - ' . $count . '<br>';
		//oprettet et nyt element i det 2-dimentionelle array med lille bogstav (letter) som index og antal forekomster som 'value'
		array_push($charlist, array('id'=> $ch1, 'value' => $count));
		$ch1++;	//opskriver til n�ste lille bogstav
		$ch2++; //opskriver til n�ste store bogstav

	}

	//retunerer array'et med bogstaver og v�rdier
	return $charlist;
	}

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //Sorterer array - (fundet p� nettet)
		function sortByOccurence($a,$b)  
		{
			return ($a["value"] <= $b["value"]) ? -1 : 1;
		}
		usort($parsedFile, "sortByOccurence");
  
		// Finder de to medianv�rdier og returnerer i et array
		// NB! Er dog usikker p� om hvad &-tegnet i paramentren betyder - om det egentlig er v�rdierne fra testfilen som overf�res og s� skulle kontrolleres op mod resultatet, men jeg har valgt at retunere resultatet.
		$occurrences = array(
			array ('letter' => $charlist[12]['letter'], 'value' => $charlist[12]['value']),
			array ('letter' => $charlist[13]['letter'], 'value' => $charlist[13]['value'])
			);

		return $occurences;
    }
}
?>